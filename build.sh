#!/usr/bin/env bash

#######################
#
#     IMAGE NAME
#
#######################
BUILD_NAME=jndev/ds-env:alpine



DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


docker build -t ${BUILD_NAME} ${DIR}

echo ""
while true; do
    read -p "Push into Dockerhub? [y/n]" yn
    case $yn in
        [Yy]* ) docker push ${BUILD_NAME}; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo ""