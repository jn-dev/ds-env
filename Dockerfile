FROM alpine

RUN set -x && \
	apk update && \
	apk add --no-cache -t .deps bash docker openssh tzdata py-pip coreutils &&  \
	rm -rf /var/cache/apk/*


RUN pip install docker-compose

RUN touch /config.yml

#Nastaveni timezone
RUN cp /usr/share/zoneinfo/Europe/Prague /etc/localtime

ADD ./src /src
ADD ./core /core
ADD ./libs /libs

RUN mkdir /env_scripts
ENV DF__GENERATED_SCRIPT_PATH=/env_scripts

WORKDIR /src/scripts

CMD ["bash", "/src/entrypoint.sh"]