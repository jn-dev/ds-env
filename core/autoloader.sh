#!/usr/bin/env bash

. /libs/printer.sh


if [ -z "$PROJECT_NAME" ];	then
		printer_printError " PROJECT_NAME must by set in environment variable. Example \"docker-compose run --rm -e PROJECT_NAME=my-project env\" "
		exit
fi

function core_config_yaml_to_variables() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

function core_config_parese_to_env_file() {

	if [ -f /env.sh ]; then
		rm -rf /env.sh
	fi

	core_config_yaml_to_variables /src/config.yml "config_"| while read -r line ; do
		echo "export $line" >> /env.sh
	done

	if [ -f /src/config.user.yml ]; then
		core_config_yaml_to_variables /src/config.user.yml "config_"| while read -r line ; do
			echo "export $line" >> /env.sh
		done
	fi

}

function core_scripts_createEnvScript ()
{
	local CMD="$1"
	local FILE="$2"

	SERVER_ALIAS=$(config "install_docker_server_alias")
	checkSubShell

	local SCRIPT="${SERVER_ALIAS} project run $PROJECT_NAME --entrypoint=\"${CMD}\""


	echo "" >> ${FILE}
	echo "${SCRIPT}" >> ${FILE}

	BUILD_GID=$(printenv "config_install_permissions_gid")
	BUILD_CHMOD=$(printenv "config_install_permissions_chmod")

	#CHOWN
	if [[ "${BUILD_GID}" == '' ]]; then
		_GID="root"
	else
		_GID=${BUILD_GID}
	fi
	chown ${UID}:${_GID} ${FILE}


	#CHMOD
	if [[ "${BUILD_CHMOD}" == '' ]]; then
		_CHMOD=770
	else
		_CHMOD=${BUILD_CHMOD}
	fi
	chmod ${_CHMOD} ${FILE}


	printer_printLine  " - create script: ${FILE_NAME}"

}

function core_install_scripts () {

	rm -rf ${DF__GENERATED_SCRIPT_PATH}/*

		printer_printSubHeadLine "Install scripts:"

		printenv | grep config_install_scripts | while read -r line ; do
			VAR_NAME="$(echo $line| cut -d'=' -f 1)"
			FILE_NAME="${VAR_NAME/config_install_scripts_/}"
			CMD=$(echo $line| cut -d'=' -f 2)
			FILE="${DF__GENERATED_SCRIPT_PATH}/$FILE_NAME"

			if [ ! -f ${FILE} ]; then
				core_scripts_createEnvScript "$CMD" "$FILE"
			fi
		done

		printer_printLine ""
}


#########################################
#
#     VAR=$(config "environment")
#
#########################################
function config {
	VAR=$(printenv "config_$1")
	if [ -z "$VAR" ];
	then
		printer_printError "Proměnná '$1' nebyla v configu nalezena" >&2
		exit 1 >&2;
	else
		echo $VAR
	fi
}


#########################################
#
#     VAR=$(config "environment") & checkSubShell
#
#########################################
function checkSubShell
{
	if [ $? -ne 0 ]; then
	    exit;
	fi
}

#AUTOLOAD CONFIG FILE
core_config_parese_to_env_file

#REGISTR CONFIG FILES IN TO ENV VARIABLES
eval $(cat /env.sh)
rm -rf /env.sh

#ENV VARIABLES
export VOLUME_BASE_PATH="${LOCAL_DS_PATH}/volumes/${PROJECT_NAME}"

export BUILD_PATH="/environment/${PROJECT_NAME}"