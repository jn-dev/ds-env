#!/usr/bin/env bash

. /libs/printer.sh

#########################################
#
#     template_generate "apache_config.cfg"  "/build/primat/src/conf/apache_config.cfg"  "ADMIN_EMAIL='${ADMIN_EMAIL}' DOCUMENT_ROOT='/var/www/application/' LOG_DIR='/log'"
#
#########################################
function template_generate ()
{
	local TEMPLATE="${1}"
	local OUTPUT_FILE="${2}"
	eval $3

	if [ -f ${OUTPUT_FILE} ]; then
		rm -rf ${OUTPUT_FILE}
	fi

eval "cat > ${OUTPUT_FILE} << EOF
$(cat /src/templates/${TEMPLATE})
EOF"

	printer_printLine " - generated ${TEMPLATE}"
}