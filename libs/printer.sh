#!/usr/bin/env bash

function printer_repeat ()
{
	printf "$1"'%.s' $(eval "echo {1.."$(($2))"}");
}

function printer_printLine ()
{
	local MESSAGE=$1
	echo "${MESSAGE}"
}

function printer_printSubHeadLine ()
{
	local MESSAGE=$1
	local SIGN="="

	printer_printLine ""
	echo "${MESSAGE}"
	printer_printLine $(printer_repeat $SIGN ${#MESSAGE})
}

function printer_printHeadLine ()
{
	local MESSAGE=$1
	local SIGN="="
	local MESSAGE_EXPANDED="$SIGN     $MESSAGE     $SIGN"
	local MESSAGE_EXPANDED_EMPTY="$SIGN     $(printer_repeat ' ' ${#MESSAGE})     $SIGN"

	printer_printLine ""
	printer_printLine $(printer_repeat $SIGN ${#MESSAGE_EXPANDED})
	printer_printLine "${MESSAGE_EXPANDED_EMPTY}"
	printer_printLine "${MESSAGE_EXPANDED}"
	printer_printLine "${MESSAGE_EXPANDED_EMPTY}"
	printer_printLine $(printer_repeat $SIGN ${#MESSAGE_EXPANDED})
	printer_printLine ""

}

function printer_printError ()
{
	local MESSAGE="ERROR: $1"
	local SIGN="!"
	local MESSAGE_EXPANDED="$SIGN     $MESSAGE     $SIGN"
	local MESSAGE_EXPANDED_EMPTY="$SIGN     $(printer_repeat ' ' ${#MESSAGE})     $SIGN"

	printer_printLine ""
	printer_printLine $(printer_repeat $SIGN ${#MESSAGE_EXPANDED})
	printer_printLine "${MESSAGE_EXPANDED_EMPTY}"
	printer_printLine "${MESSAGE_EXPANDED}"
	printer_printLine "${MESSAGE_EXPANDED_EMPTY}"
	printer_printLine $(printer_repeat $SIGN ${#MESSAGE_EXPANDED})
	printer_printLine ""

}

#########################################
#
#     RESULT=$(printer_question "Chceš pokračovat v instalaci?" "Ano[A] / Ne[n]: " "declare -A choices=( ['A']='true' ['n']='false' )" )
#
#########################################
function printer_question ()
{
	eval $3
	while true; do

		printer_printLine "$1" >&2
		read -p "$2" answer
		VALUE=${choices[$answer]}

		if [ -z "$VALUE" ];
		then

			printer_printLine "Neplatná odpověď " >&2
			printer_printLine "" >&2
		else
			echo $VALUE & break
		fi
	done
	printer_printLine "" >&2
}

#########################################
#
#     RESULT=$(printer_askForVariable "Zadej cestu k adresáři")
#
#########################################
function printer_askForVariable {
	local QUESTION="$1 "

	while true; do
		echo "" >&2
	    read -p "$QUESTION" answer
	    RESULT=$(printer_question "Bylo zadáno '${answer}' je to správně?" "Ano[A] / Ne[n]: " "declare -A choices=( ['A']='true' ['n']='false' )" )
		case $RESULT in
	        true ) echo $answer & break;;
	    esac
	done
}